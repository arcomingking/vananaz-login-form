import React from 'react';
import { View, Button, TextInput, StyleSheet, Text, Image, KeyboardAvoidingView } from 'react-native';

const loginForm = props => {
  return (
    <KeyboardAvoidingView
      style={styles.login}
      behavior="position"
    >
      <View style={styles.logo}>
        <Image
            source={require('../assets/images/logo.png')}
        />
      </View>
      <Text style={styles.textLabel}>Email</Text>            
      <TextInput
        style={styles.inputText}
        placeholder="Input email address"
        underlineColorAndroid="transparent"
        blurOnSubmit={false} 
        autoFocus={false} 
        autoCorrect={false} 
        autoCapitalize="none" 
        keyboardType="email-address" 
        returnKeyType="next"
      />
      <Text style={styles.textLabel}>Password</Text>
      <TextInput
        style={styles.inputText}
        placeholder="Input password"
        secureTextEntry={true}
        underlineColorAndroid="transparent"
        blurOnSubmit={false} 
        autoFocus={false} 
        autoCorrect={false} 
        autoCapitalize="none" 
      />
      <View style={{marginTop: 20}}>
        <Button
          style={styles.button}
          title="Sign In"
          onPress={props.onLogin}
          color="#714db2"
        />
       </View> 
    </KeyboardAvoidingView>
  );
}

const styles = StyleSheet.create({
    login: {
        height: '100%',
        width: '100%',
        paddingRight: 20,
        paddingLeft: 20
    },
    logo: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 60,
        marginBottom: 30,
        width: '100%'
    },
    textLabel: {
        textAlign: 'left',
        width: '100%',
        fontSize: 20,
        fontWeight: 'bold',
        marginTop: 15,
    },
    inputText: {
        borderWidth: 2,
        width: '100%',
        borderRadius: 5,
        height: 50,
        borderColor: '#714db2',
        marginTop: 5,   
        marginBottom: 10,
        paddingLeft: 10,
        paddingRight: 10,
        fontSize: 18,
    },
    button: {
        borderRadius: 30
    }
});

export default loginForm;