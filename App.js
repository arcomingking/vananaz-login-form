import React from 'react';
import { StyleSheet, Alert, View } from 'react-native';

import LoginForm from './components/LoginForm';

export default class App extends React.Component {

  login = () => {
    Alert.alert(
      'Login',
      'Login successfully',
      [{text: 'OK', onPress: () => console.log('OK Pressed')}],
      { cancelable: true }
    )
  }

  render() {
    return (
      <View
        style={styles.container}>
        <LoginForm
          onLogin={this.login}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
